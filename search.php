<?php
include('config/bdd.php');
session_start(); // Obligatoirement avant tout `echo`, `print` ou autre texte HTML.

if (isset($_POST['user'])) {
	header("Location: /profil.php?user=" . $_POST['user']);
	exit();
}

?>

<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<?php include('php_include/head.html'); ?>
	</head>
	<body>
		<?php include('php_include/menu.html'); ?>
		<div class="container">
			<header>
				<h1>Rechercher un joueur:<span></span></h1>
				<form action="search.php" method="POST">
					<label>Pseudo: </label>
					<input type="text" name="user">
					<button type="submit">Rechercher</button>
				</form>
			</header>
		</div><!-- /container -->
		<script src="js/classie.js"></script>
		<script src="js/gnmenu.js"></script>
		<script>
			new gnMenu( document.getElementById( 'gn-menu' ) );
		</script>
	</body>
</html>