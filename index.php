<?php
include('config/bdd.php');
session_start(); // Obligatoirement avant tout `echo`, `print` ou autre texte HTML.
?>

<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<?php include('php_include/head.html'); ?>
	</head>
	<body>
		<?php include('php_include/menu.html'); ?>
		<div class="container">
			<header>
				<h1>TheGuill84<span>Pannel de modération Discord</span></h1>
			</header>
		</div><!-- /container -->
		<script src="js/classie.js"></script>
		<script src="js/gnmenu.js"></script>
		<script>
			new gnMenu( document.getElementById( 'gn-menu' ) );
		</script>
	</body>
</html>