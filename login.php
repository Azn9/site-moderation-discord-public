<?php
include('config/bdd.php');
session_start(); // Obligatoirement avant tout `echo`, `print` ou autre texte HTML.
if(isset($_SESSION['login'])) {
    header('Location: /');
    exit();
}

$erreur = '';

if (!empty($_POST['email']) && !empty($_POST['pass'])) {
	$sql = 'SELECT COUNT(*) FROM users WHERE email = ?';
	$req = $bdd->prepare($sql);
	$req->execute(array($_POST['email']));

	while($row = $req->fetchColumn()) {
		$nb = $row;
	}

	if($nb == 0) $erreur = 'Utilisateur incorrect !';
	else {
		$req = $bdd->prepare('SELECT * FROM users WHERE email = ?');
		$req->execute(array($_POST['email']));
		//die(password_hash($_POST['pass'], PASSWORD_BCRYPT));

		while($row = $req->fetch()) {
			if(password_verify($_POST['pass'], $row['password'])) {
				session_start();

				// echo "email: " . $row['email'];

				$pseudo = $row['email'];

				$_SESSION['login'] = $row['id'];
				$_SESSION['pseudo'] = $pseudo;
				$_SESSION['type'] = $row['rang'];				
				
				header('Location: /');
				exit();
			} else {
				$erreur = 'Mot de passe incorrect !';
			}
		}
		}
	}
?>

<!DOCTYPE html>
<html lang="fr" class="no-js">
	<head>
		<?php include('php_include/head.html'); ?>
	</head>
	<body>
		<?php include('php_include/menu.html'); ?>
		<div class="container">
			<header>
				<h1>Se connecter :</h1>	
			
			<form action="login.php" method="post">
				<?php echo $erreur;?>
	<div>
		<label for="courriel">Pseudo :</label>
		<input type="text" id="email" name="email" required="required">
	</div>
	<div>
		<label for="courriel">Mot de passe :</label>
		<input type="password" id="pass" name="pass" required="required">
	</div>
	<div class="button">
		<button type="submit">Valider</button>
	</div>
</form>
				</header>
		</div><!-- /container -->
		<script src="js/classie.js"></script>
		<script src="js/gnmenu.js"></script>
		<script>
			new gnMenu( document.getElementById( 'gn-menu' ) );
		</script>
	</body>
</html>