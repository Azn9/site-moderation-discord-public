<?php
include("config/bdd.php");
session_start();

?>
<!DOCTYPE html>
<html lang="fr" class="no-js">
  <head>
    <?php include('php_include/head.html'); ?>
    <title>404 Error</title>
  </head>
  <body>
    <?php include('php_include/menu.html'); ?>
    <div class="container">
      <header>
        <h1>404 Error <span>La page à laquelle vous tentez d'accéder n'existe pas.</span><h1> 
        </header>
    </div><!-- /container -->
    <script src="js/classie.js"></script>
    <script src="js/gnmenu.js"></script>
    <script>
      new gnMenu( document.getElementById( 'gn-menu' ) );
    </script>
  </body>
</html>