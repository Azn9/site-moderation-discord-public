<?php
include('config/bdd.php');
session_start();
if(!isset($_SESSION['login'])) {
	header('Location: /login.php');
	exit();
}

if (isset($_GET['user'])) {
	$user = $_GET['user'];
} else {
	header('Location: /404.php');
	exit();
}

$sql = 'SELECT COUNT(*) FROM eplayer WHERE NAME = ?';
$req = $bdd->prepare($sql);
$req->execute(array($user));

while($row = $req->fetch()) {
	$nb = $row['COUNT(*)'];
}

if ($nb == 0) {
	$grade = "Jamais connecté.";
} else {
	$sqll = 'SELECT GRADE FROM eplayer WHERE NAME = ?';
	$reqq = $bdd->prepare($sqll);
	$reqq->execute(array($user));

	while ($row2 = $reqq->fetch()) {
		$grade = $row2['GRADE'];
	}
}

?>

<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<?php include('php_include/head.html'); ?>
	<style>
	table {
		width:100%;
	}
	table, th, td {
		border: 1px solid black;
		border-collapse: collapse;
	}
	th, td {
		padding: 15px;
		text-align: left;
	}
	table tr:nth-child(even) {
		background-color: #eee;
		color: black;
	}
	table tr:nth-child(odd) {
		background-color: #fff;
		color: black;
	}
	table th {
		background-color: black;
		color: white;
	}
	#table {

	}
</style>
</head>
<body>
	<?php include('php_include/menu.html'); ?>
	<div class="container">
		<header>
			<h1>Profil: <?php echo $user;?><span>Grade: <?php echo $grade; ?></span>
			</h1>
			<h2>Dernièrs reports: </h2>
			<div id="table">
			<table>
				<tr>
					<th>ID</th>
					<th>Joueur</th>
					<th>Report par</th>
					<th>Date</th>
					<th>Raison</th>
				</tr>
				<?php
				$sql3 = 'SELECT * FROM (SELECT * FROM log_report WHERE player = ? ORDER BY id DESC LIMIT 20) sub ORDER BY id DESC';
				$req3 = $bdd->prepare($sql3);
				$req3->execute(array($user));

				while ($row3 = $req3->fetch()) {
					echo "<tr>";
					echo "<td>" . $row3['id'] . "</td>";
					echo "<td>" . $row3['player'] . "</td>";
					echo "<td>" . $row3['moderator'] . "</td>";
					echo "<td>" . $row3['date'] . "</td>";
					echo "<td>" . $row3['raison'] . "</td>";
					echo "</tr>";
				}

				?>
			</table>
		</div> 
			<h2>Dernières sanctions: </h2>
			<div id="table">
			<table>
				<tr>
					<th>ID</th>
					<th>Type</th>
					<th>Joueur</th>
					<th>Modérateur</th>
					<th>Date</th>
					<th>Durée</th> 
					<th>Raison</th>
				</tr>
				<?php
				$sql2 = 'SELECT * FROM (SELECT * FROM log_sanctions WHERE player = ? ORDER BY id DESC LIMIT 20) sub ORDER BY id DESC';
				$req2 = $bdd->prepare($sql2);
				$req2->execute(array($user));

				while ($roww = $req2->fetch()) {
					echo "<tr>";
					echo "<td>" . $roww['id'] . "</td>";
					echo "<td>" . $roww['type'] . "</td>";
					echo "<td>" . $roww['player'] . "</td>";
					echo "<td>" . $roww['moderator'] . "</td>";
					echo "<td>" . $roww['date'] . "</td>";
					echo "<td>" . $roww['expire'] . "</td>";
					echo "<td>" . $roww['raison'] . "</td>";
					echo "</tr>";
				}

				?>
			</table>
		</div> 
			</header>
	</div><!-- /container -->
	<script src="js/classie.js"></script>
	<script src="js/gnmenu.js"></script>
	<script>
		new gnMenu( document.getElementById( 'gn-menu' ) );
	</script>
</body>
</html>
